# TODO: Get diff storage working
json.extract! report, :id, :file

json.case do
  @case = Case.find(report.case_id)
  json.id @case.id
  json.title @case.title
  json.description @case.description
end