json.extract! bookmark, :id, :title, :description

json.case do
  @case = Case.find(bookmark.case_id)
  json.id @case.id
  json.title @case.title
  json.description @case.description
end