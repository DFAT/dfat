json.extract! case_item, :id, :title, :description
case_owner = case_item.owner

if case_owner
  json.created_by do
    json.id case_owner.id
    json.(case_owner, :first_name, :last_name)
    json.(case_item, :created_at, :updated_at)
  end
end

json.bookmarks case_item.bookmarks
