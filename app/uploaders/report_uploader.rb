class ReportUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :small_preview do
    process :cover
    process convert: 'png'
    process resize_to_fit: [200, 200]

    def url
      super.chomp(File.extname(super)) + '.png'
    end

    def filename
      super.chomp(File.extname(super)) + '.png'
    end
  end

  version :preview do
    process :cover
    process convert: 'png'
    process resize_to_fit: [600, 500]

    def url
      super.chomp(File.extname(super)) + '.png'
    end
    
    def filename
      super.chomp(File.extname(super)) + '.png'
    end
  end

  def cover
    manipulate! do |frame, index|
      frame if index.zero?
    end
  end

  def extension_whitelist
    %w(pdf)
  end


end
