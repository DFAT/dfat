class ErrorController < ActionController::API
  def routing_error(error = 'Something went wrong, maybe wrong path?', status = :not_found, exception=nil)
    render :json => {:error => error}, :status => :not_found
  end
end
