module Api
  class CasesController < ApplicationController
    skip_before_action :authenticate_request

    def index
      @cases = Case.all
      render :index
    end

    def show
      @case_item = Case.find(params[:id])
      render partial: 'api/cases/case', locals: {case_item: @case_item}
    end

    def cases_by_user
      user = User.find(params[:user_id])
      if user
        @cases = user.owned_cases
        render :index
      else
        render json: {
            errors: user.errors.full_messages,
            status: :unprocessable_entity
        }
      end
    end


    private

    def cases_params
      params.permit(:file, :case_id)
    end
  end
end