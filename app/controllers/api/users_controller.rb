module Api
  class UsersController < ApplicationController
    # GET /users
    def index
      @users = User.all
      render json: @users.as_json(only: [:id, :first_name, :last_name, :username, :email])
    end

    # GET /users/1
    def show
      @user = User.find(params[:id])
      render json: @user.as_json(only: [:id, :first_name, :last_name, :username, :email])
    end

    # POST /users
    # TODO: Restrict this to admin only
    def create
      user = User.new(user_params)

      if user.save
        render json: {
          status: 'User created'
        }, status: :created
      else
        render json: {
          errors: user.errors.full_messages
        }, status: :bad_request
      end
    end

    private

    def user_params
      puts params
      params.require(:users).permit(:email, :password, :password_confirmation, :username)
    end
  end
end