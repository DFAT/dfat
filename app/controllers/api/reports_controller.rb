module Api
  class ReportsController < ApplicationController
    skip_before_action :authenticate_request

    def index
      @reports = CaseReport.all
      render :index
    end

    # TODO: Refactor this to actually generate the report from the case
    def create
      @report = CaseReport.create(report_params)

      if @report.save
        render partial: 'api/reports/report', locals: {report: @report}
      else
        render json: {
            errors: @report.errors.full_messages,
            status: :unprocessable_entity
        }
      end
    end

    private

    def report_params
      params.permit(:file, :case_id)
    end
  end
end