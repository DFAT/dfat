module Api
  class BookmarksController < ApplicationController
    skip_before_action :authenticate_request

    def index
      @bookmarks = Bookmark.all
      render :index
    end

    def create
      @bookmark = Bookmark.create(bookmark_params)

      if @bookmark.save
        render partial: 'api/bookmarks/bookmark', locals: {bookmark: @bookmark}
      else
        render json: {
            errors: @bookmark.errors.full_messages,
            status: :unprocessable_entity
        }
      end
    end

    def update
    end

    def delete
    end

    private

    def bookmark_params
      params.permit(:title, :description, :file, :case_id)
    end
  end
end