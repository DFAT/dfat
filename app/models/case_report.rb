class CaseReport < ApplicationRecord
  belongs_to :case
  mount_uploader :file, ReportUploader
end
