# User model for devise
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_save :downcase_email

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable
  has_many :permissions

  has_many :cases, through: :permissions, source: :permittable, source_type: 'Case' do

    def owner_of
      where('`permissions`.`bitmask` & > 0', Permission::OWNER)
    end
  end

  def downcase_email
    self.email = email.delete(' ').downcase
  end

  def owned_cases
    cases = Permission.all.where(permittable_type: 'Case').where(user_id: id)
    Case.find(cases.pluck(:permittable_id))
  end
end
