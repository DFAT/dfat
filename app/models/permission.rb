class Permission < ApplicationRecord
  belongs_to :permittable, polymorphic: true
  belongs_to :user

  ADMIN = 1
  OWNER = 2
  EXAMINER = 4
  REVIEWER = 8
end
