class Case < ApplicationRecord
  has_many :bookmarks
  has_many :case_reports
  has_many :permissions, as: :permittable
  has_many :users, through: :permissions

  def owner
    perm = permissions.where(permittable_id: 1).try(:first).try(:user)
    User.find(perm.id) if perm
  end
end
