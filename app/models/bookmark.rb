class Bookmark < ApplicationRecord
  belongs_to :case
  has_many :comments
  mount_uploader :file, FileUploader
end
