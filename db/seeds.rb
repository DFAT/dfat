# This file should contain all the record creation needed to seed the
# database with its default values.
# The data can then be loaded with the rails db:seed command
# (or created alongside the database with db:setup).
#
# Examples:
#
# movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# Character.create(name: 'Luke', movie: movies.first)
require 'httparty'

example_user = User.create(first_name: 'Example',
                           last_name: 'User',
                           email: 'example@mail.com',
                           username: 'exampleuser',
                           password: '123123123',
                           password_confirmation: '123123123',
                           confirmed_at: DateTime.now)

example_admin = User.create(first_name: 'Example',
                            last_name: 'Admin',
                            email: 'admin@mail.com',
                            username: 'admin',
                            password: 'admin',
                            password_confirmation: 'admin',
                            confirmed_at: DateTime.now)

cases = []
5.times do
  cases.append(Case.create(title: Faker::Lorem.sentence(3),
                           description: Faker::Lorem.paragraph(2)))
end

cases.each do |c|
  Permission.create(user_id: example_user.id,
                    permittable_type: 'Case',
                    permittable_id: c.id)

  20.times do
    b = Bookmark.new

    path = "/tmp/#{Faker::Crypto.md5}.jpg"
    File.open(path, 'wb') do |f|
      f.write HTTParty.get(Faker::LoremPixel.image).body
    end

    open(path) do |f|
      b.title = Faker::Lorem.sentence(3)
      b.description = Faker::Lorem.paragraph(2)
      b.file = f
      b.case_id = c.id
    end
    b.save!
    File.delete(path)
  end
end
