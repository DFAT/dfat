class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table 'permissions' do |t|
      t.integer  'permittable_id'
      t.string   'permittable_type'
      t.integer  'user_id'
      t.integer  'bitmask',                     default: 0

      t.timestamps
    end
  end
end
