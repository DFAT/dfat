class CreateCaseReports < ActiveRecord::Migration[5.0]
  def change
    create_table :case_reports do |t|
      t.boolean :is_original
      t.string :file
      t.references :case, foreign_key: true

      t.timestamps
    end
  end
end
