Rails.application.routes.draw do
  devise_for :users

  namespace 'api', defaults: { format: :json } do
    resources :users, except: [:new, :edit]
    resources :cases, except: [:new, :edit]
    resources :bookmarks, except: [:new, :edit]
    resources :reports, except: [:new, :edit]
    post 'authenticate' => 'authentication#authenticate', via: :options
  end

  get '*path', to: 'error#routing_error'
end
