import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CacheService } from 'ng2-cache';
import { CacheWrapperService } from './services/cache-wrapper/cache-wrapper.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
    ],
    providers: [
        CacheService,
        CacheWrapperService
    ]
})
export class ApiModule { }
