import { Config } from 'app/config';

export default class ApiService {
  private route: string;

  constructor(route) {
    this.route = Config.apiUrl().concat('/api/').concat(route);
  }

  all() {
    return this.route;
  }

  one(id: number | string) {
    return this.route.concat(`/${id}`);
  }
}
