import { Injectable } from '@angular/core';
import { CacheService, CacheStoragesEnum } from 'ng2-cache';
import { Http, RequestOptions, Response, Headers } from '@angular/http';

import * as pack from 'jsonpack';
import { of } from 'rxjs/observable/of';
import { TokenService } from 'app/auth/services/token/token.service';

@Injectable()
export class CacheWrapperService {
  private storageType: CacheStoragesEnum;

  constructor(private cache: CacheService, private http: Http, private token: TokenService) { }

  getKey(key: string) {
    const value = this.cache.get(key);
    if (value && value !== null) {
      return value;
    }
  }

  setKey<T>(key: string, value: T) {
    if (!this.cache.exists(key)) {
      this.cache.set(key, value);
    } else {
      const currentValue = this.cache.get(key);
      if (currentValue !== value) {
        this.cache.set(key, value);
      }
    }
  }

  getFromApi(route: string, authNeeded: boolean, key?: string) {
    let options;

    if (authNeeded) {
      const headers = new Headers({ 'Authorization': `Bearer ${this.token.token}`});
      options = new RequestOptions({headers: headers});
    }

    if (key) {
      if (this.cache.exists(key)) {
        return of(pack.unpack(this.cache.get(key)));
      }
    }

    return this.http.get(route, options).map((response: Response) => {
      this.cache.set(key, pack.pack(response.json()));
      console.log(response.json());
      return response.json();
    });
  }
}
