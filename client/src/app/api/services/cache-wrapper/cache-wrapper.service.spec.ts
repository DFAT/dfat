import { TestBed, inject } from '@angular/core/testing';

import { CacheWrapperService } from './cache-wrapper.service';

describe('CacheWrapperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CacheWrapperService]
    });
  });

  it('should ...', inject([CacheWrapperService], (service: CacheWrapperService) => {
    expect(service).toBeTruthy();
  }));
});
