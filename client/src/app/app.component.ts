import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @Output() navToggle = new EventEmitter<boolean>();

  navOpen() {
    this.navToggle.emit(true);
  }
  
  constructor() {
  }

  ngOnInit() {
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('currentUser') != null;
  }

  username(): string {
    if (this.isLoggedIn()) {
      const json = JSON.parse(localStorage.getItem('currentUser'));
      return json.username;
    }
  }
}
