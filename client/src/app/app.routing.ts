import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './views/home/home.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { LoginComponent } from './auth/views/login/login.component';
import { CasesComponent } from './cases/views/case/all/cases.component';
import { ReportListComponent } from './cases/components/report-list/report-list.component';
import { ProfileComponent } from './auth/components/profile/profile.component';
import { CaseComponent } from './cases/views/case/single/case.component';
import { AuthGuard } from './auth/guards/auth.guard';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'cases', component: CasesComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportListComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'cases/:id', component: CaseComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', component: NotFoundComponent }
];

export const AppRouting = RouterModule.forRoot(appRoutes);
