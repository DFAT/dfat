import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  templateUrl: './not-found.component.html',
  styleUrls:   ['./not-found.component.scss']
})
export class NotFoundComponent {
  constructor(private location: Location, private router: Router) {
  }

  goBack() {
    this.location.back();
  }

  goHome() {
    this.router.navigate(['/']);
  }
}