import { Component, OnInit } from '@angular/core';
import 'app/util';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingComponent } from 'app/components/loading/loading.component';
import { Report } from 'app/cases/models/report';
import { CaseService } from 'app/cases/services/case/case.service';
import { ReportService } from 'app/cases/services/report/report.service';
import { Case } from 'app/cases/models/case';

@Component({
  selector:    'app-home',
  templateUrl: 'home.component.html',
  styleUrls:   ['home.component.scss']
})

export class HomeComponent implements OnInit {
  cases: Case[];
  reports: Report[];

  caseLoading = true;
  reportLoading = true;

  constructor(private caseService: CaseService, private reportService: ReportService, private domSanitizer: DomSanitizer) {
    this.getCases();
    this.getReports();
  }

  ngOnInit() {
  }

  getCases() {
    this.caseService.getCases().subscribe(cases => this.cases = cases, (err) => console.error(err), () => this.caseLoading = false);
  }

  getReports() {
    this.reportService.getReports().subscribe(
      reports => this.reports = reports, (err) => console.error(err), () => this.reportLoading = false);
  }
}
