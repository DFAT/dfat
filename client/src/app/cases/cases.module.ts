import { NgModule } from '@angular/core';
import { CaseComponent } from './views/case/single/case.component';
import { CaseListComponent } from './components/case-list/case-list.component';
import { ReportListComponent } from './components/report-list/report-list.component';
import { CaseService } from './services/case/case.service';
import { ReportService } from './services/report/report.service';
import { CasesComponent } from './views/case/all/cases.component';
import { SharedModule } from 'app/shared.module';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        CaseListComponent,
        CaseComponent,
        CasesComponent,
        ReportListComponent
    ],
    providers: [
        CaseService,
        ReportService
    ],
    exports: [
        CaseListComponent,
        CaseComponent,
        CasesComponent,
        ReportListComponent
    ]
})
export class CasesModule { }
