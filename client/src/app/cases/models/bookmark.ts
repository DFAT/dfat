export class Bookmark {
  title: string;
  description: string;
  path: string;
  case_id: number;
}
