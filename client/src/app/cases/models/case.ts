import { Bookmark } from './bookmark';

export class Case {
  id: number;
  title: string;
  description: string;
  created_at: string;
  updated_at: string;
  bookmarks: Bookmark[];
}
