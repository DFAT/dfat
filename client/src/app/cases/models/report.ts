export class Report {
  file: string;
  case_id: number;
  is_original: boolean;
}
