import { Component, Input, OnInit } from '@angular/core';
import {Case} from '../../../models/case';
import {ActivatedRoute, Params} from '@angular/router';
import {CaseService} from '../../../services/case/case.service';
import { Title } from '@angular/platform-browser';
import { LoadingComponent } from 'app/components/loading/loading.component';

@Component({
  selector:    'app-case',
  templateUrl: './case.component.html',
  styleUrls:   ['./case.component.scss']
})

export class CaseComponent implements OnInit {

  @Input() case: Case;

  caseLoading = true;

  constructor(private route: ActivatedRoute, private caseService: CaseService, private titleService: Title) {
  }

  ngOnInit() {
  }
}
