import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Report } from '../../models/report';
import ApiService from 'app/api/services/api-service';
import { CacheWrapperService } from 'app/api/services/cache-wrapper/cache-wrapper.service';

@Injectable()
export class ReportService extends ApiService {

  constructor(private cacheWrapper: CacheWrapperService) {
    super('reports');
  }

  getReports(): Observable<Report[]> {
    return this.cacheWrapper.getFromApi(this.all(), true, 'reports');
  }

  getReport(id: number): Observable<Report> {
    return this.cacheWrapper.getFromApi(this.one(id), true);
  }
}
