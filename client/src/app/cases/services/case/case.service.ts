import { Case } from '../../models/case';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CacheWrapperService } from 'app/api/services/cache-wrapper/cache-wrapper.service';
import ApiService from 'app/api/services/api-service';

@Injectable()
 export class CaseService extends ApiService {

  constructor(private cacheWrapper: CacheWrapperService) {
    super('cases');
  }

  getCases(): Observable<Case[]> {
    return this.cacheWrapper.getFromApi(this.all(), true, 'cases');
  }

  getCase(id: number | string): Observable<Case> {
    return this.cacheWrapper.getFromApi(this.one(id), true);
  }
 }
