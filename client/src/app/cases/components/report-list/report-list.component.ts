import { Component, OnInit, Input } from '@angular/core';
import Util from 'app/util';
import { Report } from 'app/cases/models/report';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss']
})

export class ReportListComponent extends Util implements OnInit {
  @Input() reports: Report[];

  constructor() { super(); }

  ngOnInit() {
    console.log(this.reports);
  }

  hasReports() {
    return this.reports && this.reports.length > 0;
  }
}
