import { Component, Input, OnInit } from '@angular/core';
import { Case } from '../../models/case';
import Util from 'app/util';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.scss']
})

export class CaseListComponent extends Util implements OnInit {
  @Input() cases: Case[];

  constructor(private router: Router, private titleService: Title) { super(); }

  ngOnInit() {
    this.titleService.setTitle('Home');
  }

  navigateCase(id: number) {
    this.router.navigate([`cases/${id}`]);
  }
}
