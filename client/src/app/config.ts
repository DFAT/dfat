export class Config {
  public static apiUrl() {
    return 'http://localhost:3000';
  }

  public static toApi(route: string) {
    return this.apiUrl().concat('/api/').concat(route);
  }
}
