import { Config } from './config';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

export default class ComponentHelper {
  relativeToAbsolute(path: string) {
    return Config.apiUrl().concat(path);
  }

  asObservable<T>(subject: Subject<T>) {
    return new Observable(fn => subject.subscribe(fn));
  }
}
