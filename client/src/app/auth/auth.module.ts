import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared.module';
import { ProfileComponent } from './components/profile/profile.component';
import { LoginComponent } from './views/login/login.component';
import { AuthenticationService } from './services/authentication/authentication.service';
import { TokenService } from './services/token/token.service';
import { UserService } from './services/user/user.service';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        ProfileComponent,
        LoginComponent
    ],
    providers: [
        AuthenticationService,
        TokenService,
        UserService,
        AuthGuard
    ]
})
export class AuthModule { }
