import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {User} from '../../models/user';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Config } from 'app/config';
import { TokenService } from '../../services/token/token.service';

@Component({
  selector:    'app-login',
  templateUrl: './login.component.html',
  styleUrls:   ['./login.component.css']
})

export class LoginComponent implements OnInit {
  model: User = new User();
  loading = false;
  error = '';
  private userUrl: string;


  constructor(private http: Http, private router: Router, private authService: AuthenticationService, private token: TokenService) {
    this.userUrl = Config.apiUrl().concat('/api/users');
  }

  ngOnInit() {
    this.authService.logout();
  }

  // TODO: This needs cleaning up
  login() {
    this.loading = true;

    this.authService.login(this.model.email, this.model.password).subscribe((result) => {
      if (result === true) {
        const userHeaders = new Headers({ 'Authorization': `Bearer ${this.token.token}`});
        const options = new RequestOptions({headers: userHeaders});
        this.http.get(this.userUrl, options).subscribe((resp: Response) =>  {
          const users: User[] = resp.json();
          for (const user of users) {
            if (user.email === this.model.email) {
              localStorage.setItem('currentUser', JSON.stringify(user));
            }
          }
        });
        this.router.navigate(['/']);
      } else {
        this.error = 'Username or password is incorrect';
        this.loading = false;
      }
    });
  }

}
