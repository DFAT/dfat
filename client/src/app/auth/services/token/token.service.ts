import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  private _token: string;

  constructor() { }

  get token() {
    if (!this._token || this._token == null) {
      this._token = localStorage.getItem('token');
    }
    return this._token;
  }

  set token(token: string) {
    this._token = token;
    localStorage.setItem('token', this.token);
  }

  clear() {
    this._token = null;
    localStorage.removeItem('token');
  }
}
