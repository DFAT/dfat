import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {User} from 'app/auth/models/user';
import ApiService from 'app/api/services/api-service';
import { TokenService } from 'app/auth/services/token/token.service';

@Injectable()
export class UserService extends ApiService {
  private userUrl: string;

  constructor(private http: Http, private token: TokenService) {
    super('users');
  }

  getUsers(): Observable<User[]> {
    const headers = new Headers({ 'Authorization': `Bearer ${this.token.token}`});
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.userUrl, options).map((response: Response) => response.json());
  }
}
