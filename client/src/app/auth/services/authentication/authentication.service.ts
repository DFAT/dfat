import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Config} from 'app/config';
import 'rxjs/add/operator/map';
import { TokenService } from '../token/token.service';

@Injectable()
export class AuthenticationService {
  private authUrl: string;

  constructor(private http: Http, private token: TokenService) {
    this.authUrl = Config.apiUrl().concat('/api/authenticate');
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  login(email: string, password: string): Observable<boolean> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authUrl, JSON.stringify({
      email: email,
      password: password
    }), {headers: headers}).map((response: Response) => {
      const token = response.json() && response.json().auth_token;
      if (token) {
        this.token.token = token;
        return true;
      } else {
        return false;
      }
    });
  }

  logout(): void {
    this.token.clear();
    localStorage.removeItem('currentUser');
  }
}
