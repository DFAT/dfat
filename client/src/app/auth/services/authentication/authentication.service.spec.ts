/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import {HttpModule} from '@angular/http';
import { TokenService } from '../token/token.service';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        AuthenticationService,
        TokenService
      ]
    });
  });

  it('should construct, login and logout', inject([AuthenticationService, TokenService],
    (authenticationService: AuthenticationService, tokenService: TokenService) => {
    expect(authenticationService).toBeTruthy();
    const value = authenticationService.login('example@mail.com', '123123123');
    value.subscribe((result: Boolean) => {
      expect(result).toBe(true);
      expect(tokenService.token).toBeDefined();
      authenticationService.logout();
      expect(tokenService.token).toBeNull();
    });
  }));
});
