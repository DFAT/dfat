import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import {AppRouting} from './app.routing';

import 'hammerjs';
import { CacheService } from 'ng2-cache';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { ApiModule } from './api/api.module';
import { AuthModule } from './auth/auth.module';
import { CasesModule } from './cases/cases.module';
import { SharedModule } from './shared.module';

@NgModule({
  declarations: [
    HomeComponent,
    NotFoundComponent,
    AppComponent
  ],
  imports: [
    CasesModule,
    SharedModule,
    ApiModule,
    AuthModule,
    AppRouting
  ],
  providers: [
    CacheService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
